/*
 ============================================================================
 Driver Name : UART_DEV
 Author      : ROMAN SKAKUN
 License     : GPL
 Description : Low level char serial driver - Ransberry pi 
 ============================================================================
*/

#include "uart.h"
#include "uart_ioctl.h"
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/circ_buf.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/uaccess.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <asm/io.h>
#include <linux/workqueue.h>


// Info developer this module driver

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ROMAN SKAKUN");


/* If we enable debug messages please uncomment this def.*/

//#define UART_DEBUG


#define FIRST_MINOR 0
#define NR_DEVICES  1

/* Buffer configurations */
#define READ_BUFF_SIZE 		128
#define WRITE_BUFF_SIZE 	128

/* Configurations  addresses*/

#define NR_PORTS  	8       // numbers ports dev
#define BASE_ADDR 	0X3F8   // base address dev


#define THR 		(BASE_ADDR + 0)		/* Transmitter Holding Register (Write Only) */
#define RBR 		(BASE_ADDR + 0)		/* Receiver Buffer Register (Read Only) */
#define DLAB_L 		(BASE_ADDR + 0)		/* Divisor Latch Register :: Low */
#define IER 		(BASE_ADDR + 1)		/* Interrupt Enable Register (Read Only) */
#define DLAB_H 		(BASE_ADDR + 1)		/* Divisor Latch Register :: High */
#define IIR 		(BASE_ADDR + 2)		/* Interrupt Identity Register (Read Only) */
#define FCR 		(BASE_ADDR + 2)		/* FIFO Control Register (Write Only) */


/* Device structure */
struct uart_port 
{
	struct cdev cdev;
	struct device *device;
	/* Declarate buffers read and write */
	struct circ_buf wr_buff, rd_buff;
	spinlock_t rd_slock, wr_slock;
	wait_queue_head_t wr_waitq, rd_waitq;
	atomic_t open_cnt;
	spinlock_t open_slock;

	unsigned int irq;

	/* Debug info */
	unsigned int baud;
	unsigned long int tx_cnt, rx_cnt; // counters of buffers indexes
};

struct uart_port port[NR_DEVICES];

struct class *uart_class;


/* sysfs attribute to show the statistics */
ssize_t stat(struct device *dev, struct device_attribute *attribute, char *buffer)
{
	struct uart_port *port = (struct uart_port *)dev_get_drvdata(dev);

	return sprintf(buf,   "Tx: %lu\nRx: %lu\n"
			      "head= %d, tail = %d\n"
			      "head = %d tail = %d\n"
			      "Current rate = %u\n",
      							port->tx_cnt, port->rx_cnt,
							port->rd_buff.head, port->rd_buff.tail,
							port->wr_buff.head, port->wr_buff.tail,
							port->baud);
}

// set attribute device
DEVICE_ATTR(stat, 0666, stat, NULL);

/* Function to return the space available and buffer count of a circular buffer*/
int buff_space(struct circ_buf *cbuff, unsigned int size)
{
	return CIRC_SPACE(cbuff->head, cbuff->tail, size);
}

int buff_cnt(struct circ_buf *cbuff, unsigned int size)
{
	return CIRC_CNT(cbuff->head, cbuff->tail, size);
}


void tx_chars(struct uart_port *port)
{
	char ch;

	spin_lock(&port->wr_slock);

	/* Transmit the data */
	while (buff_cnt(&port->wr_buff, WRITE_BUFF_SIZE)) {

		ch = port->wr_buff.buf[port->wr_buff.tail];

		/* Wait until the previous transmission is completed */
		while (!(inb_p(LSR) & 0x20));

		/* Transmit one byte */
		outb_p(ch, THR);

		port->tx_cnt++;

		port->wr_buff.tail = (port->wr_buff.tail + 1) & (WRITE_BUFF_SIZE - 1);

#ifdef UART_DEBUG
		INFO("TX : %c\n", ch);
#endif
	}

	spin_unlock(&port->wr_slock);

	/* Disable the transmit interrupt */
	outb_p(inb_p(IER) & 0xFD, IER);

	/* Wake up the processes that are waiting on the buffer */
	wake_up_interruptible(&port->wr_waitq);
}


// Recieve char from device
void rx_char(struct uart_port *port)
{
	char ch;
	/* Accept the data */
	ch = inb_p(RBR);
	port->rx_cnt++;

#ifdef UART_DEBUG
	INFO("RX: %c\n", ch);
#endif

	/* Check if there is space in the buffer. If not, discard the byte */
	if (buff_space(&port->rd_buff, READ_BUFF_SIZE) == 0) return;

	/* Push the data to circular buffer */
	port->rd_buff.buf[port->rd_buff.head] = ch;
	port->rd_buff.head = (port->rd_buff.head + 1) & (READ_BUFF_SIZE - 1);

	/* Wake the data */
	wake_up_interruptible(&port->rd_waitq);
}



/* This handler produce executive irq event from device */

irqreturn_t uart_isr(int irq, void *devid)
{
	struct uart_port *port = devid;

	char int_pend = inb_p(IIR);   // calculate irq event

#ifdef UART_DEBUG
	INFO("From ISR\n");
#endif

	/* Check this device*/
	if (int_pend & 0x01) return IRQ_NONE;

	while (!(int_pend & 0x01))
	{
		/* receive data available  */
		if (inb_p(RBR) & 0X01) {
#ifdef UART_DEBUG
			INFO("New data received interrupt!\n");
#endif
			rx_char(port);
		}

		/*  transmitter empty interrupt */
		if (inb_p(FCR) & 0X20) {
#ifdef UART_DEBUG
			INFO("TX empty!\n");
#endif
			tx_chars(port);
		}

		int_pend = inb_p(IIR);
	}

	return IRQ_HANDLED;
}


// Read data from device
ssize_t uart_read(struct file *filp, char __user *ubuff, size_t cnt, loff_t *off)
{
	int i = 0;
	char ch;
	unsigned long flags;
	struct uart_port *port = filp->private_data;

	spin_lock_irqsave(&port->rd_slock, flags);

	while (buff_cnt(&port->rd_buff, READ_BUFF_SIZE) == 0) {

		spin_unlock_irqrestore(&port->rd_slock, flags);

		if (filp->f_flags & O_NONBLOCK) return -EAGAIN;

		if (wait_event_interruptible(port->rd_waitq, buff_cnt(&port->rd_buff, READ_BUFF_SIZE))) return -ERESTARTSYS;

		spin_lock_irqsave(&port->rd_slock, flags);
	}

	cnt = min((int)cnt, buff_cnt(&port->rd_buff, READ_BUFF_SIZE));

	for (i = 0; i < cnt; i++) {

		ch = port->rd_buff.buf[port->rd_buff.tail];
		port->rd_buff.tail = (port->rd_buff.tail + 1) & (READ_BUFF_SIZE - 1);
		put_user(ch, &ubuff[i]);

#ifdef UART_DEBUG
		INFO("RD: %c\n", ch);
#endif
	}

	spin_unlock_irqrestore(&port->rd_slock, flags);
	return i;
}

// Write to device
ssize_t uart_write(struct file *filp, const char __user *ubuff, size_t cnt, loff_t *off)
{
	int i = 0;
	char ch;
	unsigned long flags;
	struct uart_port *port = filp->private_data;

	/* Acquire the lock */
	spin_lock_irqsave(&port->wr_slock, flags);

	/* Wait until there is space in the buffer */
	while (buff_space(&port->wr_buff, WRITE_BUFF_SIZE) == 0) {
		if (filp->f_flags & O_NONBLOCK) return -EAGAIN;
		if (wait_event_interruptible(port->wr_waitq, buff_space(&port->wr_buff, WRITE_BUFF_SIZE))) return -ERESTARTSYS;
		spin_lock_irqsave(&port->wr_slock, flags);
	}

	cnt = min((int)cnt, buff_space(&port->wr_buff, WRITE_BUFF_SIZE));

	/* Write the data into the buffer */
	for (i = 0; i < cnt; i++) {

		get_user(ch, &ubuff[i]);

#ifdef UART_DEBUG
		INFO("Writing : %c\n", ch);
#endif

		port->wr_buff.buf[port->wr_buff.head] = ch;
		port->wr_buff.head = (port->wr_buff.head + 1) & (WRITE_BUFF_SIZE - 1);
	}

	spin_unlock_irqrestore(&port->wr_slock, flags);

	/* Enable the write buffer empty interrupt */
	outb_p(inb_p(IER) | 0x02, IER);

	return i;
}

// Device open 

int uart_open(struct inode *inode, struct file *filp)
{
	struct uart_port *port = container_of(inode->i_cdev, struct uart_port, cdev);

	filp->private_data = port;

	spin_lock(&port->open_slock);

	atomic_inc(&port->open_cnt);

	if (atomic_read(&port->open_cnt) == 1) {

		if (request_irq(port->irq, uart_isr, IRQF_SHARED, "uart_int", port)) {

			spin_unlock(&port->open_slock);
			return -EBUSY;
		}

		/* Enable receive data available interrupt */
		outb_p(inb_p(IER) | 0x01, IER);
	}

	spin_unlock(&port->open_slock);

	return 0;
}


// Device close
int uart_close(struct inode *inode, struct file *filp)
{
	struct uart_port *port;
	port = filp->private_data;

	spin_lock(&port->open_slock);

	if(atomic_dec_and_test(&port->open_cnt)) {

		/* Disable the receive data available interrupt */
		outb_p(inb_p(IER) & 0XFE, IER);

		free_irq(port->irq, port);
	}

	spin_unlock(&port->open_slock);

	return 0;
}



//------ Static initialize functions of device

static int __init uart_init(void)
{
	int i, result;
	dev_t temp_dev;

	/* Create a char driver */
	result = alloc_chrdev_region(&dev_num, FIRST_MINOR, NR_DEVICES, "serial_driver");
	if (result) {

		PERR("driver alloc failed\n");
		return result;
	}

	/* Create a class corresponding to this driver */
	uart_class = class_create(THIS_MODULE, "uart");
	if (!uart_class) {

		PERR("Class creation failed\n");
		result = -EINVAL;
		goto class_fail;
	}

	/* Iterate over all the devices and initialise */
	for (i = 0; i < NR_DEVICES; i++) {

		/* Create char device */
		temp_dev = MKDEV(MAJOR(dev_num), FIRST_MINOR + i);
		cdev_init(&port[i].cdev, &uart_fops);
		result = cdev_add(&port[i].cdev, temp_dev, 1);
		if (result) {

			PERR("Cdev creation failed\n");
			goto cdev_fail;
		}

		/* Create new device*/
		port[i].device = device_create(uart_class, NULL, temp_dev, &port[i], "uart%d", i);
		if (!port[i].device) {

			PERR("Failed to create the device\n");
			result = -EINVAL;
			goto device_fail;
		}

		/* Create a statistic file */
		device_create_file(port[i].device, &dev_attr_stat);

		/* Initialise the buffers */
		port[i].wr_buff.buf = kzalloc(WRITE_BUFF_SIZE, GFP_KERNEL);
		if (!port[i].wr_buff.buf) {

			PERR("Failed to allocate memory for write buffer\n");
			result = -ENOMEM;
			goto wr_fail;
		}

		port[i].rd_buff.buf = kzalloc(READ_BUFF_SIZE, GFP_KERNEL);
		if (!port[i].rd_buff.buf) {

			PERR("Failed to allocate memory for read buffer\n");
			result = -ENOMEM;
			goto rd_fail;
		}

		/* Initialise the wait queues */
		init_waitqueue_head(&port[i].wr_waitq);
		init_waitqueue_head(&port[i].rd_waitq);

		/* Initialise the UART port */
		result = port_init(&port[0]);
	
		if (result) goto ports_fail;
	}


	INFO("Driver Load\n");

	return 0;

	/* Error exec*/
	ports_fail: 	kfree(port[i - 1].rd_buff.buf);
	rd_fail: 	kfree(port[i].wr_buff.buf);
	wr_fail: 	device_destroy(uart_class, temp_dev);
	device_fai: 	cdev_del(&port[i].cdev);
	cdev_fail: 	class_destroy(uart_class);
	class_fail: 	unregister_chrdev_region(dev_num, NR_DEVICES);

	return result;
}

static void __exit uart_exit(void)
{
	int i;
	dev_t temp_dev;

	/* Release the ports device */
	release_region(BASE_ADDR, NR_PORTS);

	/* all USED devices unallocate*/
	for (i = 0; i < NR_DEVICES; i++) {

		kfree(port[i].wr_buff.buf);  // CLEAR BUFFERS
		kfree(port[i].rd_buff.buf);

		temp_dev = MKDEV(MAJOR(dev_num), FIRST_MINOR + i); // TAKE DEV

		device_remove_file(port[i].device, &dev_attr_stat);
		device_destroy(uart_class, temp_dev);		// DELETE DEVICE

		cdev_del(&port[i].cdev);
	}

	/* Destroy the class */
	class_destroy(uart_class);

	/* Unregister the driver */
	unregister_chrdev_region(dev_num, NR_DEVICES);

	INFO("Driver Removed \n");
}
module_init(uart_init)
module_exit(uart_exit)

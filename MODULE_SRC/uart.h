// Main includes header files 
#include<linux/init.h>
#include<linux/module.h>

/* Define abstract names of out messages */

#define DRIVER_NAME "UART_DEV"     	// name  driver 
#define DEBUG_MSG(format,args...) 	printk(DEBUG_MODULE"%s:"fmt,	DRIVER_NAME, 	##args)
#define INFO(fmt,args...) 		printk(KERN_INFO_DEBUG"%s:"fmt,	DRIVER_NAME, 	##args)
#define PERR(fmt,args...) 		printk(KERN_ERR_INFO"%s:"fmt,	DRIVER_NAME,	##args)



// ----------------------- End of file -----------------------------------------

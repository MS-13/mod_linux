#include <stdio.h>
#include <fcntl.h>
#include <linux/ioctl.h>
#include "MODULE_SRC/uart_ioctl.h"


int main(int argc, char *argv[])
{
	int fd;

	if (argc != 2) 
	{

		printf("Used</dev/uartx>\n");
		return 1;
	}

	fd = open(argv[1], O_RDWR);  
	if (fd < 0) 
	{

		printf("Unable to open the file\n");
		return 2;
	}

	/* init uart */
	ioctl(fd, BAUD_9600);
	close(fd);
	return 0;
}
